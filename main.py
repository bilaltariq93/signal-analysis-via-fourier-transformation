
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.fftpack import fft, ifft


def annot_max(x, y, ax=None):
    yv = (y.argsort()[-1:][::-1]).tolist()

    labelX = [0.4, 1.05, 1.05]
    labelY = [0.95, 0.4, 0.8]

    for v in range(len(yv)):
        xmax = x[yv[v]]
        ymax = y[yv[v]]

        text = "Frequency # " + str(v + 1) + " = {:.2f}".format(xmax, ymax)
        if not ax:
            ax = plt.gca()
        bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
        arrowprops = dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=90")
        kw = dict(xycoords='data', textcoords="axes fraction",
                  arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
        ax.annotate(text, xy=(xmax, ymax), xytext=(labelX[v], labelY[v]), **kw)

def ApplyingFourierTransformation(df1):
    # We have received an ndarray (2D array)

    # We will use fft function which will use measurements in data to compute discrete Fourier Transform.
    # We have also normalize amplitude
    df1[:, 1] = df1[:, 1] - np.median(df1[:, 1])
    ft_ = fft(np.array(df1[:, 1])) #max(df1[:, 1])

    # computing an array of the frequency bin centers in cycles per unit of the sample spacing
    # Spacing sample is static in the current data, hence I have just used the first two data points.
    frequencies = np.fft.fftfreq(ft_.size, d=df1[:, 0][1] - df1[:, 0][0])

    # Removing opposite phase frequncies
    arg = np.argwhere(frequencies < 0)
    frequencies = np.delete(frequencies, arg)

    # print(np.abs(np.median(ft_)))

    # Rescaling amplitudes array with the frequency array's length
    ft_ = np.delete(ft_, arg)
    #ft_ = np.abs(ft_) - np.abs(np.median(ft_))
    #ft_ = np.round(ft_, 1)

    #Since we need to need 3 signal from the data, we will select Top 3 signals and plot a sine wave.
    topThreeAmp = (np.abs(ft_[(np.abs(ft_).argsort()[-3:][::-1]).tolist()])).tolist()
    topThreeFreq = (frequencies[(np.abs(ft_).argsort()[-3:][::-1]).tolist()]).tolist()

    #To estimate noise, we will use all values excluding the the top 3. Assumption is all others are noise.
    ExcludingTopThreeAmp = list()

    for value in range(len(ft_)):
        a = np.abs(ft_[value])
        if a in topThreeAmp:
            ExcludingTopThreeAmp.append(0)
        else:
            ExcludingTopThreeAmp.append(a)

    DrawSignal(topThreeFreq[0], 0.5, 3, len(ft_), 'Highest Freq. Sine Wave', topThreeAmp[0])
    CombinedDrawSignal(topThreeFreq, 0.5, 4, len(ft_), 'Aggregate of Top Three Frequencies from FFT', topThreeAmp)

    plt.figure(2)
    plt.title('Using Fourier transformation to estimate frequency')
    plt.xlabel('Frequency')
    plt.ylabel('Amplitude')
    plt.plot(frequencies, np.abs(ft_)/ np.max(np.abs(ft_)))
    annot_max(frequencies, np.abs(ft_), plt)
    print('Figure ' + str(2) + ' represents Fourier transformation result to estimate frequency.')

    plt.figure(6)
    plt.title('Noise')
    plt.xlabel('Frequency')
    plt.ylabel('Amplitude')
    plt.plot(frequencies, ExcludingTopThreeAmp/np.max(ExcludingTopThreeAmp))
    print('Figure ' + str(5) + ' represents Noise.')


def DrawSignal(freq, timeInterval, fig, maxTimeInterval, title, amp):
    t = np.arange(0, maxTimeInterval, timeInterval)
    y = amp * np.sin(2 * np.pi * freq * t)
    plt.figure(fig)
    plt.title(title)
    plt.plot(t, y / max(y))
    plt.xlabel('Time')
    plt.ylabel('Amplitude')
    print('Figure ' + str(fig) + ' represents '+ title)

def CombinedDrawSignal(freq, timeInterval, fig, maxTimeInterval, title, amp):
    t = np.arange(0, maxTimeInterval, timeInterval)
    y = 0
    for x in range(len(amp)):
        y = y + (amp[x] * np.sin(2 * np.pi * freq[x] * t))

    #y = amp * np.sin(2 * np.pi * freq * t)
    plt.figure(fig)
    plt.title(title)
    plt.plot(t, y / max(y))
    plt.xlabel('Frequency')
    plt.ylabel('Amplitude')
    print('Figure ' + str(fig) + ' represents '+ title)


def main():
    # Enter the FileName (Along with Path)
    fileName = 'SelfAssesment5.csv'

    try:
        # Reads CSV as Data frame
        df1 = pd.read_csv(fileName, skiprows=1, names=('t_in_s', 'measurement'))
    except:
        print('File path or format is invalid. Code will exit with -1')
        exit(-1)

    # Converting the Data Frame into ndArray
    df1 = df1.values

    # Ploting the orignal File (Measurement vs Time line graph)
    plt.figure(1)
    plt.title(fileName)
    plt.plot(df1[:, 0], np.abs(df1[:, 1] / np.max(df1[:, 1])))
    print('Figure ' + str(1) + ' represents the data provided.')


    # This method will read the time series data
    # And file hidden signals using Fourier Transformation
    ApplyingFourierTransformation(df1)
    plt.show()


if __name__ == "__main__":
    main()
